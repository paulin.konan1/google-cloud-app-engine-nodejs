# GitLab Multicloud - Google Cloud App Engine

Deploy a Node.js app from a Git repo to Google Cloud App Engine via GitLab Pipelines.

## Minimalist Node App

This demo uses a minimalist Node app, but this concept works with all other runtimes supported by Google App Engine including Java, Python, Ruby, C#, Go, PHP etc.

## Secure Pipeline

Pipelines in GitLab are defined as code. The `.gitlab-ci.yml` file at project root contains the source.

Pipeline-as-code means best practice pipelines can be templatized and reused across several teams.

The following [GitLab Secure](https://docs.gitlab.com/ee/user/application_security/) scanners are included as part of this pipeline:

1. Static Application Security Tests
1. Secret Detection
1. Dependency Scanning
1. Container Scanning

These ensure that vulnerabilities do not leak into the `main` branch when developing with Merge Requests and the GitLab flow.

## Deployment

The `gcloud` CLI is used for deploying to Google Cloud - as it is the standardized method for Google Cloud automation.

`gcloud` is authorized with the Google App Engine project's Service Account, which is downloaded as a key file and make available to the GitLab Runner.

## Feedback

If you have questions or wish to provide feedback, please [create an issue](https://gitlab.com/sri-at-gitlab/demos/multi-cloud-with-gitlab/google-cloud-app-engine/-/issues/new).
